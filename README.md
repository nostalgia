Long, long time ago, I got interested in programming and wondered what
programming language to pick.  I tried out TurboPascal first, but
couldn't get it to run, so VB6 and VBScript it was.  My original
creations were joke programs that played tricks on their user.  The
[windows93.net] website and its many demos inspired me to recreate
them as faithfully as possible, but with more modern technologies.  As
there isn't exactly a port of VB6 for Linux, I picked GTK, but paired
it with JavaScript because that's what someone new to programming in
2017 would use.  You'll therefore need to install `gjs` to run them.
Have fun!

[windows93.net]: http://windows93.net/
